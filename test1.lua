--test 1
--niuliqiang
require 'torch'

torch.manualSeed(1234)

N = 5

A = torch.rand(N, N)

A = A*A:t()

A:add(0.001, torch.eye(N))

b = torch.rand(N)

function J(x)
	return 0.5*x:dot(A*x)-b:dot(x)
end

print("test1")
print(J(torch.rand(N)))

xs = torch.inverse(A)*b

print("test2")
print(string.format('J(x^*) = %g', J(xs)))

function dJ(x)
	return A*x-b
end

x = torch.rand(N)

lr = 0.01
for i=1, 20000 do
	x = x - dJ(x)*lr
	-- we print the value of the objective function at each iteration
	print(string.format('at iter %d J(x) = %f', i, J(x)))
end

--using th eoptim pkg

do
	local neval = 0
	function JdJ(x)
		local Jx = J(x)
		neval = neval + 1
		print(string.format('after %d evaluations J(x) = %f', neval, Jx))
		return Jx, dJ(x)
	end
end

require 'optim'

state = {
	verbose = true,
	maxIter = 100
}

x = torch.rand(N)
optim.cg(JdJ, x, state)


--plot
evaluations = {}
time = {}
timer = torch.Timer()
neval = 0
function JdJ(x)
	local Jx = J(x)
	neval = neval + 1
	print(string.format('after %d evaluation, J(x) = %f', neval, Jx))
	table.insert(evaluations, Jx)
	table.insert(time, timer:time().real)
	return Jx, dJ(x)
end

state = {
	verbose = true,
	maxIter = 100
}

x0 = torch.rand(N)
cgx = x0:clone()
timer:reset()
optim.cg(JdJ, cgx, state)

cgtime = torch.Tensor(time)
cgevaluations = torch.Tensor(evaluations)

--SGD
evaluations = {}
time = {}
neval = 0
state = {
	lr = 0.1
}

x = x0:clone()

timer:reset()

for i=1,1000 do
	optim.sgd(JdJ, x, state)
end

sgdtime = torch.Tensor(time)
sgdevaluations = torch.Tensor(evaluations)

--final plot
require 'gnuplot'

gnuplot.figure(1)
gnuplot.plot(cgtime, cgevaluations)

gnuplot.figure(2)
gnuplot.plot(sgdtime, sgdevaluations)

--
gnuplot.pngfigure('plot.png')
gnuplot.plot(
	{'CG', cgtime, cgevaluations, '-'},
	{'SGD', sgdtime, sgdevaluations, '-'})
gnuplot.xlabel('time (s)')
gnuplot.ylabel('J(x)')
gnuplot.plotflush()
--end
